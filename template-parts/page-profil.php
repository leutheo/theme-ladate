<section class="section">
    <div class="columns is-centered">
      <div class="column is-one-quarter">
        <div class="card">
          <div class="card-image">
            <figure class="image is-4by3">
              <?php 
                $variable = wp_get_attachment_url(9);
                // $url = wp_get_attachment_url(9);
              ?>  
              <img src="<?php echo get_template_directory_uri(); ?>/img/treb.jpg" alt="Placeholder image">
              <!-- <img src="<?php echo $url; ?> alt="Placeholder image"> -->
              <!-- <?php echo wp_get_attachment_image(9, Array(800,600));?> -->
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-left">
                <figure class="image is-48x48">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/treb.jpg" alt="Placeholder image">
                </figure>
              </div>
              <div class="media-content">
                <p class="title is-4">Mark Ladate</p>
                <p class="subtitle is-6">Auteur de la trilogie</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="profil column is-one-quarter">
        <p class="title is-4">Le profil de l'auteur</p>
        <p>Considérant la difficulté de ces derniers temps, nous sommes contraints d'inventorier la somme des voies de bon sens, parce qu'il est temps d'agir.</p>  
        <p>Avec la politique de l'époque actuelle, nous sommes contraints de caractériser la globalité des ouvertures de bon sens, avec beaucoup de recul.</p>            
        <p>Compte tenu de complexité actuelle, il faut favoriser la simultanéité des options emblématiques, parce que les mêmes causes produisent les mêmes effets.</p>            
        <a href="#">#moustache</a> <a href="#">#sexy</a>  
      </div>

    </div>
  </section>