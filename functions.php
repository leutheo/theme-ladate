<?php
    add_action( 'wp_enqueue_scripts', 'enqueue_styles_ladate' );
    function enqueue_styles_ladate() {
        wp_enqueue_style( 'style-ladate', get_template_directory_uri().'/style.css' );
        wp_enqueue_style( 'style-principal', get_template_directory_uri().'/css/main.css' );
    }
?>