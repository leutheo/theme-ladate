<?php
    get_header();
    get_template_part("template-parts/page","header");
    get_template_part("template-parts/page","profil");
    get_template_part("template-parts/page","books");
    get_template_part("template-parts/page","critiques");
    get_template_part("template-parts/page","commandes");

?>
  
<?php
    get_footer();
?>